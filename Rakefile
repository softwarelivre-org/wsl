# encoding: UTF-8

require 'yaml'
require 'erubis'

yaml = Dir.glob('*/*.yaml').sort.reverse
YEARS = yaml.map { |yaml| File.dirname(yaml) }.sort

def load_proceedings(file)
  proceedings = YAML.load_file(file)
  year = File.basename(File.dirname(file))
  proceedings['year'] ||= year
  proceedings['papers'].each do |paper|
    paper['year'] ||= year
    paper['code'] = "%04d" % paper['code']
  end
  proceedings
end

html = yaml.map { |yaml| "public/#{File.dirname(yaml)}/index.html" }
html_template = 'templates/index.html.erb'
search_template = 'templates/search.html.erb'
paper_template = 'templates/paper.html.erb'
download_template = 'templates/download.html.erb'
assets = Dir.glob('assets/**/*').reject { |f| File.directory?(f) }.map { |f| "public/#{f}"}
index = 'public/index.html'
search = 'public/search/index.html'
paper_pages = []
yaml.each do |edition|
  load_proceedings(edition)['papers'].each do |paper|
    page = "public/#{paper['year']}/#{paper['code']}/index.html"
    download_page = "public/#{paper['year']}/#{paper['code']}/download/index.html"
    pdf = "public/#{paper['year']}/#{paper['code']}/#{paper['file']}"
    paper_pages << page << download_page << pdf
    rule page => [edition, paper_template] do |t|
      FileUtils::Verbose.mkdir_p(File.dirname(t.name))
      root = '../..'
      write_template(t.name, paper_template, binding)
    end
    rule download_page => [edition, download_template] do |t|
      FileUtils::Verbose.mkdir_p(File.dirname(t.name))
      root = '../../..'
      write_template(t.name, download_template, binding)
    end
    rule pdf => File.join(File.dirname(edition), paper['file']) do |t|
      FileUtils::Verbose.mkdir_p(File.dirname(t.name))
      FileUtils::Verbose.ln(t.source, t.name)
    end
  end
end

upload = html + assets + [index] + [search] + paper_pages

def markdown2html(template_file)
  `pandoc -f markdown -t html #{template_file}`
end

def analytics
  File.read('templates/analytics.html.erb')
end

def write_template(output_file, template_file, binding = nil)

  # ----------------------------------
  proceedings = nil
  root = '.'
  # ----------------------------------
  binding ||= binding()

  template = File.read(template_file)
  processor = Erubis::Eruby.new(template)
  text = processor.result(binding)
  FileUtils.mkdir_p(File.dirname(output_file))
  File.open(output_file, 'w') do |f|
    f.write(text)
    if block_given?
      yield
    else
      puts "#{output_file} written."
    end
  end
end

rule /public\/[0-9]+\/index.html/ => [ lambda { |out| File.join(File.dirname(out), 'data.yaml').sub(/^public\//, '') }, lambda { |out| File.join(File.dirname(out), 'committee.markdown').sub(/^public\//, '') }, html_template ] + yaml do |t|
  proceedings = load_proceedings(t.source)
  root = '..'
  committee = markdown2html(File.join(File.dirname(t.source), 'committee.markdown'))

  write_template t.name, html_template, binding() do
    puts "#{t.source} -> #{t.name}"
  end
end

rule /public\/assets\// => lambda { |pdf| pdf.sub(/^public\//, '') } do |t|
  if !File.exists?(t.name)
    FileUtils::mkdir_p(File.dirname(t.name))
    FileUtils::Verbose.ln(t.source, t.name)
  end
end

file search => [search_template] + yaml do
  root = '..'
  papers = []

  yaml.each do |t|
    load_proceedings(t).tap do |proceedings|
      proceedings['papers'].each do |paper|
        papers << paper
      end
    end
  end

  write_template search, search_template, binding()
end

file index => [html_template] + yaml do |t|
  write_template t.name, html_template
end

file html => yaml

desc "Removes generated files"
task :clean do
  rm_rf 'public'
end

desc 'Builds the entire thing'
task :default => upload

desc 'uploads to the server'
task :upload => ['upload:nonhtml', 'upload:html']

if ENV['USER'] == 'terceiro'
  rsync_target = 'people.softwarelivre.org:/srv/wsl/'
else
  rsync_target = 'terceiro@people.softwarelivre.org:'
end

desc 'uploads only non-HTML files'
task 'upload:nonhtml' => :default do
  sh "rsync -avp --delete --exclude=*.html public/ #{rsync_target}"
end

desc 'upload only HTML files'
task 'upload:html' => :default do
  sh "rsync -avp --delete public/ #{rsync_target}"
end

desc 'input file for tag cloud generation'
file 'tagcloud.txt' => yaml do |t|
  text = []
  yaml.each do |filename|
    data = YAML.load_file(filename)
    data['papers'].each do |paper|
      text << paper['title']
      text << paper['abstract']
    end
  end
  File.open(t.name, 'w') do |f|
    f.puts(text)
  end
end
